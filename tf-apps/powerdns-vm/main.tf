terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

#
# Configure the OpenStack Provider
# 
provider "openstack" {
  user_name   = var.openstack_user
  tenant_name = var.openstack_tenant
  password    = var.openstack_password
  auth_url    = var.openstack_auth_url
  region      = var.openstack_region
}

#
# Network configuration
#
resource "openstack_networking_network_v2" "network" {
  name           = var.ressources_base_name
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet" {
  name       = var.ressources_base_name
  network_id = openstack_networking_network_v2.network.id
  cidr       = var.openstack_subnet_cidr
  ip_version = 4
}

data "openstack_networking_network_v2" "extnetwork" {
  name = var.openstack_existing_extnet
}

resource "openstack_networking_router_v2" "router" {
  name           = var.ressources_base_name
  admin_state_up = "true"
  external_network_id = data.openstack_networking_network_v2.extnetwork.id
}

resource "openstack_networking_router_interface_v2" "int" {
  router_id = openstack_networking_router_v2.router.id
  subnet_id = openstack_networking_subnet_v2.subnet.id
}

resource "openstack_compute_secgroup_v2" "secgroup" {
  name        = var.ressources_base_name
  description = "PowerDNS security group"

  rule {
    from_port   = 8081
    to_port     = 8081
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 53
    to_port     = 53
    ip_protocol = "udp"
    cidr        = "0.0.0.0/0"
  }
}

#
# Instance creation
#

data "openstack_images_image_v2" "debian" {
  name        = "debian-12"
  most_recent = true
}

data "openstack_compute_flavor_v2" "medium" {
  name = "m1.small"
}

resource "random_password" "password" {
  length           = 32
  special          = false
}

resource "openstack_compute_instance_v2" "instance" {
  depends_on = [ openstack_networking_subnet_v2.subnet ]
  name            = var.ressources_base_name
  image_id        = data.openstack_images_image_v2.debian.id
  flavor_id       = data.openstack_compute_flavor_v2.medium.id
  security_groups = [openstack_compute_secgroup_v2.secgroup.name]

  network {
    name = openstack_networking_network_v2.network.name
  }
  user_data       = <<EOT
#cloud-config
hostname: david.example.com
fqdn: instance_1.example.com
timezone: Europe/Paris
manage_etc_hosts: true
resize_rootfs: true

package_upgrade: true
packages:
- pdns-server
- sqlite3
- pdns-backend-sqlite3

write_files:
- content: |
    include-dir=/etc/powerdns/pdns.d
    launch=gsqlite3
    gsqlite3-database=/var/lib/powerdns/pdns.sqlite3
    security-poll-suffix=
    webserver=true
    webserver-address=0.0.0.0
    webserver-allow-from=0.0.0.0/0
    api=yes
    api-key=${random_password.password.result}
  path: /etc/powerdns/pdns.conf

runcmd:
- systemctl disable systemd-resolved.service
- systemctl stop systemd-resolved
- sqlite3 /var/lib/powerdns/pdns.sqlite3 < /usr/share/doc/pdns-backend-sqlite3/schema.sqlite3.sql
- chown -R pdns:pdns /var/lib/powerdns
- systemctl restart pdns

EOT
}

resource "openstack_networking_floatingip_v2" "fip" {
  pool = var.openstack_existing_extnet
}

resource "openstack_compute_floatingip_associate_v2" "fip" {
  floating_ip = openstack_networking_floatingip_v2.fip.address
  instance_id = openstack_compute_instance_v2.instance.id
}

output "api_url" {
    value = "http://${openstack_networking_floatingip_v2.fip.address}:8081"
}
output "api_token" {
    value = random_password.password.result
    sensitive = true
}
output "dns_server_ip" {
    value = "${openstack_networking_floatingip_v2.fip.address}"
}