# PowerDNS VM

This terraform receipt create a PowerDNS server in an Openstack cluster.

This server is not reachable by SSH and only provided an API for the
configuration and the DNS port.

## Warning / Disclailmer

This is a demo server to explore Terraform controller possibilities. DO NOT USE
IN PRODUCTION.

API is not secured with HTTPS. The better would be to add a reverse-proxy with a
correct SSL certificate.

## Openstack Ressources created

- A network
- A subnetwork
- A router
- A security group
- A debian12 instance with PowerDNS server installed and an API configured with
  a random password
- A floating IP

## Openstack Requirements

- An existing openstack tenant with credentials
- An external network
- A debian 12 image called `debian-12`
- A flavor `m1.small`

## Terraform vars

| name                      | required | description                        |
|---------------------------|----------|------------------------------------|
| openstack_user            | true     | Openstack credentials informations |
| openstack_tenant          | true     | Openstack credentials informations |
| openstack_password        | true     | Openstack credentials informations |
| openstack_auth_url        | true     | Openstack credentials informations |
| openstack_region          | true     | Openstack credentials informations |
| openstack_existing_extnet | true     | Openstack existing external network to attach |
| ressources_base_name      | true     | Ressources name to create |
| openstack_subnet_cidr     | true     | Network CIDR to create |

## Terraform Outputs

- api_url: the http url to connect to the API
- api_token: the generated token