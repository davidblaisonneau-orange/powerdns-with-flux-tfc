# Openstack credentials

openstack_user      = "user"
openstack_tenant    = "tenant"
openstack_password  = "password"
openstack_auth_url  = "https://my.open.stack:5000"
openstack_region    = "RegionOne"
openstack_existing_extnet = "extnet0"

# VM parameters

ressources_base_name = "powerdns_001"
openstack_subnet_cidr = "192.168.199.0/24"