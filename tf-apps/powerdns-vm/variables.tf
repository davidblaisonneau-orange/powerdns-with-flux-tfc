variable "openstack_user" {
    type = string
}

variable "openstack_tenant" {
    type = string
}

variable "openstack_password" {
    type = string
}

variable "openstack_auth_url" {
    type = string
}

variable "openstack_region" {
    type = string
    default = "RegionOne"
}

variable "ressources_base_name" {
    type = string
}

variable "openstack_existing_extnet" {
    type = string
    description = "the extnet to attach the router"
}

variable "openstack_subnet_cidr" {
    type = string
    description = "The CIDR of the subnet to create"
}