# PowerDNS records

This terraform receipt configure a PowerDNS server.
It creates Zones and Records based on a TF variable.

## Requirements

- An existing powerDNS server with a configured API

## Terraform vars

| name                      | required | description                        |
|---------------------------|----------|------------------------------------|
| api_url            | true     | URL to connect PowerDNS API |
| api_token          | true     | Token to connect to PowerDNS |
| records        | true     | A list of records to create (see `example.tfvars`)

## Terraform Outputs

None