terraform {
required_version = ">= 0.14.0"
  required_providers {
    powerdns = {
      source = "pan-net/powerdns"
      version = "1.5.0"
    }
  }
}

provider "powerdns" {
  api_key    = var.api_token
  server_url = var.api_url
}

variable "api_token" {
  type = string
  default = "changeme"
}

variable "api_url" {
  type = string
}

variable "records" {
  type = string
  default = "[]"
}

locals{
  zones = toset(distinct(flatten([
    for zone in jsondecode(var.records) : [
      zone.zone
    ]
  ])))
  records = toset(distinct(flatten([
    for zone in jsondecode(var.records) : [
      for record in zone.records : {
        zone = zone.zone
        type = lookup(record, "type", "A")
        source = record.source
        target = record.target
        ttl = lookup(record, "ttl", "300")
      }
    ]
  ])))
}

resource "powerdns_zone" "zone" {
  for_each    = local.zones
  name        = each.value
  kind        = "Native"
  nameservers = ["ns1.${each.value}", "ns2.${each.value}"]
}

resource "powerdns_record" "record" {
  for_each    = {for record in local.records:  record.source => record}
  zone        = each.value.zone
  name        = "${each.value.source}.${each.value.zone}"
  type        = each.value.type
  ttl         = each.value.ttl
  records     = each.value.target
  depends_on = [powerdns_zone.zone]
}
