---
apiVersion: v1
kind: Secret
metadata:
  name: openstack-creds
stringData:
  openstack_user: "******"
  openstack_tenant: "******"
  openstack_password: "******"
  openstack_auth_url: "******"
  openstack_region: "******"
  openstack_existing_extnet: "******"
